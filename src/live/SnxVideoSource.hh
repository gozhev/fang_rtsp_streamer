/* © Mikhail Gozhev <dev@gozhev.ru> / Summer 2018 / Moscow, Russia */
#ifndef _SNX_VIDEO_SOURCE_HH
#define _SNX_VIDEO_SOURCE_HH

#include <thread>
#include <condition_variable>
#include <memory>
#include <queue>
#include <string>

#include <sys/time.h>

#include <isp_lib_api.h>
#include <snx_vc_lib.h>
#include <snx_rc_lib.h>

#include <FramedSource.hh>

class SnxVideoSource: public FramedSource
{
public:
	static SnxVideoSource *createNew(UsageEnvironment &env,
			struct snx_m2m *m2m);
	std::string const &get_aux_sdp_line() { return aux_sdp_line; }

protected:
	SnxVideoSource(UsageEnvironment& env, struct snx_m2m *m2m);
	virtual ~SnxVideoSource();

	/*
	 * redefined virtual functions
	 */
protected:
	virtual void doGetNextFrame();
	//virtual void doStopGettingFrames();

private:
	enum worker_event_t { NONE, QUIT, COLD_START, FRAME_HANDLED };
	worker_event_t worker_event;
	mutable std::mutex worker_event_mutex;
	mutable std::condition_variable worker_event_cond;

	std::thread worker;

	volatile bool frame_pending;
	mutable std::condition_variable frame_pending_cond;
	mutable std::mutex frame_pending_mutex;

	EventTriggerId handle_frame_trigger;
	EventTriggerId device_error_trigger;

	struct snx_m2m *m2m;
	std::unique_ptr<struct snx_rc> rate_ctl;

	struct frame_t {
		typedef std::unique_ptr<uint8_t[]> data_ptr;
		data_ptr data;
		size_t size;
		struct timeval ts;
		frame_t():
			data{},
			size{},
			ts{}
		{}
		frame_t(frame_t &&o):
			data(std::move(o.data)),
			size(o.size),
			ts(o.ts)
		{}
	};
	std::queue<struct frame_t> frame_buf;
	size_t frame_buf_max_size;

	std::string sps;
	std::string pps;
	std::string aux_sdp_line;

private:
	void worker_func();

	static void deliver_frame(void *client_data);
	void deliver_frame();
	static void device_error(void *client_data);
	void device_error();
	void enqueue_frame();

	int skip_markers(char unsigned *&data, size_t &data_size,
			char unsigned *&frame, size_t &frame_size);
	int set_worker_event(worker_event_t value);

	bool is_frame_pending() const
	{
		std::unique_lock<std::mutex> lock(frame_pending_mutex);
		return frame_pending;
	}

	void set_frame_pending(bool value)
	{
		std::unique_lock<std::mutex> lock(frame_pending_mutex);
		frame_pending = value;
	}
};

#endif
