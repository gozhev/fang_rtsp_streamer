/* © Mikhail Gozhev <dev@gozhev.ru> / Summer 2018 / Moscow, Russia */
#include "SnxVideoSource.hh"

#include <cstdlib>
#include <sstream>
#include <iomanip>

#include <Base64.hh>

SnxVideoSource *SnxVideoSource::createNew(UsageEnvironment &env,
		struct snx_m2m *m2m)
{
	return new SnxVideoSource(env, m2m);
}

SnxVideoSource::SnxVideoSource(UsageEnvironment& env,
		struct snx_m2m *m2m_):
	FramedSource(env),
	worker_event(NONE),
	frame_pending(false),
	handle_frame_trigger(0),
	device_error_trigger(0),
	m2m(m2m_),
	rate_ctl(new struct snx_rc()),
	frame_buf_max_size(5),
	sps(),
	pps(),
	aux_sdp_line()
{
	int ret;

	m2m->isp_fd = snx_open_device(m2m->isp_dev);
	ret = snx_isp_init(m2m);
	if (ret) {
	}

	m2m->codec_fd = snx_open_device(m2m->codec_dev);
	ret = snx_codec_init(m2m);
	if (ret) {
	}

	rate_ctl->width = m2m->width;
	rate_ctl->height = m2m->height;
	rate_ctl->codec_fd = m2m->codec_fd;
	rate_ctl->Targetbitrate = m2m->bit_rate;
	rate_ctl->framerate = m2m->codec_fps;
	rate_ctl->gop = m2m->gop;
	m2m->qp = snx_codec_rc_init(rate_ctl.get(), SNX_RC_INIT);

	ret = snx_isp_start(m2m);
	if (ret) {
	}

	system("echo 0x1 > /proc/isp/drc/enable");

	/*
	 * if fps_ctrl = 0: isp drop frame
	 * if fps_ctrl = 1: use AE exposure time to output average fps
	 */
	system("echo 0 > /proc/isp/ae/fps_ctrl");

	ret = snx_codec_start(m2m);
	if (ret) {
	}

	handle_frame_trigger = envir().taskScheduler().createEventTrigger(
			deliver_frame);
	device_error_trigger = envir().taskScheduler().createEventTrigger(
			device_error);

	set_worker_event(COLD_START);
	worker = std::thread(&SnxVideoSource::worker_func, this);
}

SnxVideoSource::~SnxVideoSource()
{
	int ret;

	set_worker_event(QUIT);
	worker.join();

	envir().taskScheduler().deleteEventTrigger(handle_frame_trigger);
	envir().taskScheduler().deleteEventTrigger(device_error_trigger);

	ret = snx_codec_stop(m2m);
	ret = snx_isp_stop(m2m);

	ret = snx_codec_uninit(m2m);
	ret = snx_isp_uninit(m2m);

	ret = ::close(m2m->codec_fd);
	ret = ::close(m2m->isp_fd);
}

/*
 * Send an event to the worker thread.
 * If there is a (non-NONE) pending event already set the function fails.
 * The QUIT event however has the highest priority and overwrites any other
 * events.
 */
int SnxVideoSource::set_worker_event(worker_event_t value)
{
	std::unique_lock<std::mutex> lock(worker_event_mutex);
	if (value != QUIT && worker_event != NONE)
		return 1;
	worker_event = value;
	lock.unlock();
	worker_event_cond.notify_one();
	return 0;
}

void SnxVideoSource::worker_func()
{
	int ret;
	worker_event_t ev;

	int read_fail_count = 0;
	int const read_fail_limit = 3;

	for (;;) {
		std::unique_lock<std::mutex> worker_event_lock(
				worker_event_mutex);
		while (worker_event == NONE)
			worker_event_cond.wait(worker_event_lock);
		ev = worker_event;
		worker_event = NONE;
		worker_event_lock.unlock();
		if (ev == QUIT)
			break;
		if (ev == COLD_START)
			goto cold_start;
//		if (ev == FRAME_HANDLED)
//			goto restart;
restart:
		ret = snx_codec_reset(m2m);
cold_start:
		ret = snx_codec_read(m2m);
		if (!m2m->cap_bytesused) {
			if (++read_fail_count > read_fail_limit) {
				fprintf(stderr, "codec read failed\n");
				envir().taskScheduler().triggerEvent(
						device_error_trigger, this);
				set_worker_event(QUIT);
				continue;
			}
			goto restart;
		}
		read_fail_count = 0;

		m2m->qp = snx_codec_rc_update(m2m, rate_ctl.get());

//		set_frame_pending(true);
		enqueue_frame();
//		set_frame_pending(false);
		set_worker_event(FRAME_HANDLED);
	}
}

void SnxVideoSource::device_error(void *client_data)
{
	((SnxVideoSource *)client_data)->device_error();
}

void SnxVideoSource::device_error()
{
	/*
	 * Note: If, for some reason, the source device stops being readable
	 * (e.g., it gets closed), then you do the following:
	 */
	handleClosure();
}

void SnxVideoSource::enqueue_frame()
{
	///* there is no immediately available data */
	//if (!is_frame_pending())
	//	return;

	char unsigned *data =
		(char unsigned *) m2m->cap_buffers[m2m->cap_index].start;
	size_t data_size = m2m->cap_bytesused;
	char unsigned *frame = NULL;
	size_t frame_size = 0;

	while (!skip_markers(data, data_size, frame, frame_size)) {
		frame_t f{};

		f.data = frame_t::data_ptr(new uint8_t[frame_size]);
		memcpy(f.data.get(), frame, frame_size);
		f.size = frame_size;
		f.ts = m2m->timestamp;

		while (frame_buf.size() >= frame_buf_max_size)
			frame_buf.pop();

		frame_buf.push(std::move(f));

		if (!sps.empty() && !pps.empty())
			break;

		switch (frame[0] & 0x1F) {
			case 7:
				sps.assign((char *)frame, frame_size);
				break;
			case 8:
				pps.assign((char *)frame, frame_size);
				break;
			default:
				break;
		}

		if (aux_sdp_line.empty() && !sps.empty() && !pps.empty()) {
			uint32_t profile_level_id = 0;
			if (sps.size() >= 4)
				profile_level_id = (sps[1] << 16)
					| (sps[2] << 8) | sps[3];

			char *sps_base64 =
				base64Encode(sps.c_str(), sps.size());
			char *pps_base64 =
				base64Encode(pps.c_str(), pps.size());

			std::ostringstream sout;
			sout << "profile-level-id=";
			sout << std::hex << std::setw(6) << profile_level_id;
			sout << ";sprop-parameter-sets=";
			sout << sps_base64 << "," << pps_base64;
			aux_sdp_line.assign(sout.str());

			free(sps_base64);
			free(pps_base64);
		}
	}

	//deliver_frame();
	envir().taskScheduler().triggerEvent(handle_frame_trigger, this);
}

int SnxVideoSource::skip_markers(char unsigned *&data, size_t &data_size,
		char unsigned *&frame, size_t &frame_size)
{
	/* H264 marker */
	char const marker[] = {0, 0, 0, 1};
	size_t marker_size = sizeof(marker);

	if (data_size < marker_size) {
		return 1;
	}
	if (memcmp(data, marker, marker_size)) {
		return 1;
	}
	data += marker_size;
	data_size -= marker_size;

	frame_size = data_size;
	frame = data;

	if (sps.empty() || pps.empty()) {
		for (size_t n = 0; n < data_size - marker_size; ++n) {
			if (!memcmp(data + n, marker, marker_size)) {
				frame_size = n;
				break;
			}
		}
	}

	data += frame_size;
	data_size -= frame_size;

	return 0;
}

void SnxVideoSource::deliver_frame(void *client_data)
{
	((SnxVideoSource *)client_data)->deliver_frame();
}

void SnxVideoSource::deliver_frame()
{
	/* we're not ready for the data yet */
	if (!isCurrentlyAwaitingData())
		return;

	if (frame_buf.empty())
		return;

	frame_t const &f = frame_buf.front();

	/*
	 * deliver the data here:
	 */
	if (f.size > fMaxSize) {
		fFrameSize = fMaxSize;
		fNumTruncatedBytes = f.size - fMaxSize;
	} else {
		fFrameSize = f.size;
	}

	fPresentationTime = f.ts;

	memmove(fTo, f.data.get(), fFrameSize);

	frame_buf.pop();

	/*
	 * After delivering the data, inform the reader
	 * that it is now available
	 */
	FramedSource::afterGetting(this);
}

/*
 * This function is called (by our 'downstream' object)
 * when it asks for new data.
 */
void SnxVideoSource::doGetNextFrame()
{
	/*
	 * If a new frame of data is immediately available to be delivered,
	 * then do this now:
	 */
	deliver_frame();

	/*
	 * If no new data is immediately available to be delivered we don't do
	 * anything more here. Instead, our event trigger must be called (e.g.,
	 * from a separate thread) when new data becomes available.
	 */
}

