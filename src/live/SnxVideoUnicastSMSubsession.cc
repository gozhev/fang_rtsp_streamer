/* © Mikhail Gozhev <dev@gozhev.ru> / Summer 2018 / Moscow, Russia */
#include "SnxVideoUnicastSMSubsession.hh"

#include <H264VideoStreamDiscreteFramer.hh>
#include <H264VideoStreamFramer.hh>
#include <H264VideoRTPSink.hh>

#include "SnxVideoSource.hh"

#include <cstring>
#include <sstream>
#include <iostream>
#include <cassert>

SnxVideoUnicastSMSubsession *SnxVideoUnicastSMSubsession::createNew(
		UsageEnvironment& env, struct snx_m2m *m2m)
{
	return new SnxVideoUnicastSMSubsession(env, m2m);
}

SnxVideoUnicastSMSubsession::SnxVideoUnicastSMSubsession(
		UsageEnvironment& env, struct snx_m2m *m2m):
	OnDemandServerMediaSubsession(env, true),
	m2m(m2m),
	aux_sdp_line(),
	asl_done_flag(0),
	dummy_sink(NULL),
	dummy_source(NULL)
{
}

SnxVideoUnicastSMSubsession::~SnxVideoUnicastSMSubsession()
{
}

FramedSource *SnxVideoUnicastSMSubsession::createNewStreamSource(
		unsigned clientSessionId, unsigned &estBitrate)
{
	auto source = SnxVideoSource::createNew(envir(), m2m);
	return H264VideoStreamDiscreteFramer::createNew(envir(), source);
}

RTPSink *SnxVideoUnicastSMSubsession::createNewRTPSink(Groupsock *rtpGroupsock,
		unsigned char rtpPayloadTypeIfDynamic,
		FramedSource *inputSource)
{
	return H264VideoRTPSink::createNew(envir(), rtpGroupsock,
			rtpPayloadTypeIfDynamic);
}

void SnxVideoUnicastSMSubsession::after_playing_dummy(void *client_data)
{
	static_cast<SnxVideoUnicastSMSubsession *>(
			client_data)->after_playing_dummy();
}

void SnxVideoUnicastSMSubsession::after_playing_dummy()
{
	envir().taskScheduler().unscheduleDelayedTask(nextTask());
	set_asl_done_flag();
}

void SnxVideoUnicastSMSubsession::check_for_aux_sdp_line(void *client_data)
{
	static_cast<SnxVideoUnicastSMSubsession *>(
			client_data)->check_for_aux_sdp_line();
}

void SnxVideoUnicastSMSubsession::check_for_aux_sdp_line()
{
	nextTask() = NULL;

	if (!aux_sdp_line.empty()) {
		set_asl_done_flag();
		return;
	}

	if (dummy_sink && dummy_source
			&& !dummy_source->get_aux_sdp_line().empty()) {
		std::ostringstream sout;
		sout << "a=fmtp:"
			<< (int) dummy_sink->rtpPayloadType()
			<< " " << dummy_source->get_aux_sdp_line()
			<< "\r\n";

		aux_sdp_line = sout.str();
		dummy_sink = NULL;
		dummy_source = NULL;
		set_asl_done_flag();

		return;
	}

	if (!asl_done_flag) {
		int usec = 100000; /* [microsec] */
		nextTask() =
			envir().taskScheduler().scheduleDelayedTask(usec,
					check_for_aux_sdp_line, this);
	}
}

char const *SnxVideoUnicastSMSubsession::getAuxSDPLine(RTPSink *rtpSink,
		FramedSource *inputSource)
{
	/* it's already been set up (for a previous client) */
	if (!aux_sdp_line.empty())
		return aux_sdp_line.c_str();

	/* we're not already setting it up for another, concurrent stream */
	if (dummy_sink == NULL) {
		/*
		 * Note: For H264 video files, the 'config' information
		 * ("profile-level-id" and "sprop-parameter-sets") isn't known
		 * until we start reading the file.  This means that "rtpSink"s
		 * "auxSDPLine()" will be NULL initially, and we need to start
		 * reading data from our file until this changes.
		 */
		dummy_sink = rtpSink;

		FramedFilter *filter = dynamic_cast<FramedFilter *>(
				inputSource);
		assert(filter);
		dummy_source = dynamic_cast<SnxVideoSource *>(
				filter->inputSource());

		int rc = dummy_sink->startPlaying(*inputSource,
				after_playing_dummy, this);

		check_for_aux_sdp_line();
	}
	envir().taskScheduler().doEventLoop(&asl_done_flag);


	envir() << "aux sdp line: \""
		<< aux_sdp_line.substr(0, aux_sdp_line.size()-2).c_str()
		<< "\"\n";

	return aux_sdp_line.c_str();
}

