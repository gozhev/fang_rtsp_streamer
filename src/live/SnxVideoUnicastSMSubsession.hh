/* © Mikhail Gozhev <dev@gozhev.ru> / Summer 2018 / Moscow, Russia */
#ifndef _SNX_VIDEO_UNICAST_SM_SUBSESSION_HH
#define _SNX_VIDEO_UNICAST_SM_SUBSESSION_HH

#include <string>

#include <stdlib.h>
#include <snx_vc_lib.h>

#include <OnDemandServerMediaSubsession.hh>
#include "SnxVideoSource.hh"

class SnxVideoUnicastSMSubsession: public OnDemandServerMediaSubsession
{
public:
	static SnxVideoUnicastSMSubsession *createNew(UsageEnvironment &env,
			struct snx_m2m *m2m);

protected:
	SnxVideoUnicastSMSubsession(UsageEnvironment &env, struct snx_m2m *m2m);
	virtual ~SnxVideoUnicastSMSubsession();

	/*
	 * redefined virtual functions
	 */
protected:
	virtual FramedSource *createNewStreamSource(unsigned clientSessionId,
			unsigned &estBitrate);
	virtual RTPSink *createNewRTPSink(Groupsock *rtpGroupsock,
			unsigned char rtpPayloadTypeIfDynamic,
			FramedSource *inputSource);
	virtual char const *getAuxSDPLine(RTPSink *rtpSink,
			FramedSource *inputSource);

private:
	struct snx_m2m *m2m;
	std::string aux_sdp_line;
	char asl_done_flag;
	RTPSink *dummy_sink;
	SnxVideoSource *dummy_source;

private:
	static void after_playing_dummy(void *client_data);
	void after_playing_dummy();
	static void check_for_aux_sdp_line(void *client_data);
	void check_for_aux_sdp_line();

	void set_asl_done_flag()
	{
		asl_done_flag = 1;
	}
};

#endif
