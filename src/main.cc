/* © Mikhail Gozhev <dev@gozhev.ru> / Summer 2018 / Moscow, Russia */
#include <cstdio>
#include <csignal>
#include <cstring>
#include <cerrno>

#include <linux/videodev2.h>

#include <iostream>
#include <memory>
#include <stdexcept>
#include <sstream>

#include <liveMedia.hh>
#include <BasicUsageEnvironment.hh>
#include "SnxVideoUnicastSMSubsession.hh"

static void signal_handler(int sig);
static void set_up_signal_handling(void);
static void print_session_url(RTSPServer const *rtsp_server,
		ServerMediaSession const *sms);

char volatile g_quit = 0;

int main(int argc, char **argv) try
{
	set_up_signal_handling();

	std::unique_ptr<struct snx_m2m>
		m2m(new struct snx_m2m());

	m2m->m2m = 1;
	m2m->m2m_buffers = 2;
	m2m->isp_mem = V4L2_MEMORY_MMAP;
	m2m->out_mem = V4L2_MEMORY_USERPTR;
	m2m->isp_fmt = V4L2_PIX_FMT_SNX420;
	m2m->codec_fmt = V4L2_PIX_FMT_H264;
	m2m->width = 1920;
	m2m->height = 1080;
	m2m->scale = 1;
	m2m->codec_fps = 30;
	m2m->isp_fps = 30;
	m2m->gop = 30;
	m2m->qp = 0;
	m2m->bit_rate = 1024*1024; /* [bit/sec] */
	strcpy(m2m->isp_dev, "/dev/video0");
	strcpy(m2m->codec_dev, "/dev/video1");

	char const *url_path = "unicast";

	/* bitrate*factor */
	OutPacketBuffer::maxSize = m2m->bit_rate/8 * 4; /* [bytes] */

	std::unique_ptr<TaskScheduler>
		scheduler(BasicTaskScheduler::createNew());

	auto env_deleter =
		[](UsageEnvironment *env)
		{
			int ret = (int) env->reclaim();
			if (!ret) {
				std::cerr << "failed to reclaim live555 environment"
					<< std::endl;
			}
		};

	std::unique_ptr<UsageEnvironment, decltype(env_deleter)>
		env(BasicUsageEnvironment::createNew(*scheduler),
				env_deleter);

	auto rtsp_server_deleter =
		[](RTSPServer *rtsp_server)
		{
			/*
			 * this call deletes all registered subsessions,
			 * sessions and rtsp_server itself
			 */
			Medium::close(rtsp_server);
		};

	std::unique_ptr<RTSPServer, decltype(rtsp_server_deleter)>
		rtsp_server(RTSPServer::createNew(*env, 554),
				rtsp_server_deleter);
	if (!rtsp_server) {
		std::ostringstream ss;
		ss << "failed to create rtsp server: "
			<< env->getResultMsg() << "\n";
		throw std::runtime_error(ss.str());
	}

	/* these guys are cleaned up in the rtsp server's destructor */
	ServerMediaSubsession *video_smss =
		SnxVideoUnicastSMSubsession::createNew(*env, m2m.get());
	ServerMediaSession *sms =
		ServerMediaSession::createNew(*env, url_path);

	sms->addSubsession(video_smss);
	rtsp_server->addServerMediaSession(sms);

	print_session_url(rtsp_server.get(), sms);

	env->taskScheduler().doEventLoop(&g_quit);

	return 0;
}
catch (std::exception &e) {
	std::cerr << "error: " <<  e.what() << std::endl;
}

void signal_handler(int sig)
{
	(void) sig;
	std::cerr << "request to quit\n";
	g_quit = 1;
}

void set_up_signal_handling(void)
{
	struct sigaction sa{};
	sa.sa_handler = signal_handler;
	sigemptyset(&sa.sa_mask);
	int ret = sigaction(SIGINT, &sa, NULL);
	if (ret) {
		std::ostringstream ss;
		ss << "failed to install signal handler: "
			<< strerror(errno) << "\n";
		throw std::runtime_error(ss.str());
	}
}

void print_session_url(RTSPServer const *rtsp_server,
		ServerMediaSession const *sms)
{
	std::unique_ptr<char[]> url(rtsp_server->rtspURL(sms));
	printf("play this stream using the url \"%s\"\n", url.get());
}

