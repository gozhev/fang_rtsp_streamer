# © Mikhail Gozhev <dev@gozhev.ru> / Autumn 2018 / Moscow, Russia

ELF ?= $(BUILD_DIR)/$(NAME).elf

.PHONY: elf
elf: $(ELF)

OBJS := $(patsubst %, $(BUILD_DIR)/%.o, $(SRCS))

.SECONDARY: $(OBJS)

$(ELF): $(OBJS) $(STATIC_LIBS)
	$(LD) -o $@ $^ $(LDFLAGS) $(LDLIBS)

$(filter %.c.o, $(OBJS)):\
$(BUILD_DIR)/%.o: %
	$(CC) -c -o $@ $(CFLAGS) $(CPPFLAGS) $<

$(filter %.cc.o %.cpp.o %.cxx.o %.c++.o, $(OBJS)):\
$(BUILD_DIR)/%.o: %
	$(CXX) -c -o $@ $(CXXFLAGS) $(CPPFLAGS) $<

$(filter %.S.o %.s.o %.asm.o, $(OBJS)):\
$(BUILD_DIR)/%.o: %
	$(AS) -c -o $@ $(ASFLAGS) $(CPPFLAGS) $<

.PHONY: objs
objs: $(OBJS)

#
# dirs handling block
#
ELF_DIR := $(dir $(ELF))
OBJ_DIRS := $(sort $(dir $(OBJS)))

$(ELF): | $(ELF_DIR)
$(OBJS): | $(OBJ_DIRS)
$(OBJ_DIRS): | $(BUILD_DIR)
$(STATIC_LIBS): | $(BUILD_DIR)

#
# cleanup
#
.PHONY: objs-clean
objs-clean:
	$(call rm_all, $(OBJS))

.PHONY: bin-clean
bin-clean:
	$(call rm_all, $(ELF))

#
# global hooks
#
DIRTREES += $(ELF_DIR)
DIRTREES += $(OBJ_DIRS)

clean distclean: objs-clean bin-clean

