# © Mikhail Gozhev <dev@gozhev.ru> / Autumn 2018 / Moscow, Russia

# force to call make for libraries
.PHONY: allall aa
allall: libs all
aa: allall

#
# add custom targets as prerequisites to
# the following targets
#
.PHONY: libs
libs:

.PHONY: libs-clean libs-distclean
libs-clean:
libs-distclean:
distclean: libs-distclean

#
# call 'make -C <libdir> clean|distclean' for each library;
# do not remove source-tree customizations;
# do not remove compiled libraries from the current out-of-tree build
# directory;
#
.PHONY: libs-clean-self libs-distclean-self
libs-clean-self:
libs-distclean-self:

