# © Mikhail Gozhev <dev@gozhev.ru> / Autumn 2018 / Moscow, Russia

.PHONY: tags
tags:
	ctags -R

.PHONY: tags-clean
tags-clean:
	$(call rm_all, tags)

distclean: tags-clean

