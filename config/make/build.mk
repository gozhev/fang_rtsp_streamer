# © Mikhail Gozhev <dev@gozhev.ru> / Autumn 2018 / Moscow, Russia

.DEFAULT_GOAL := all

.PHONY: all
all:

.PHONY: clean distclean
clean distclean:

DIRS += $(BUILD_DIR)

