# © Mikhail Gozhev <dev@gozhev.ru> / Autumn 2018 / Moscow, Russia

$(DIRS):
	$(call mkdir_cond, $@)

$(DIRTREES):
	$(call mkdir_p_cond, $@)

dirs-clean:
	$(call rm_empty_dirs, $(DIRS))

dirtrees-clean:
	$(call rm_empty_dirs, $(DIRTREES))

clean distclean: dirs-clean

