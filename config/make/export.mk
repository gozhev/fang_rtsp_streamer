# © Mikhail Gozhev <dev@gozhev.ru> / Autumn 2018 / Moscow, Russia

# add here variables to be exported
FOR_EXPORT +=

export $(FOR_EXPORT)

#
# print exported environment
#
.PHONY: export
export:
	@$(foreach x, $(FOR_EXPORT),\
		echo 'export $x="$($x)"' $(NL))

