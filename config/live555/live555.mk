# © Mikhail Gozhev <dev@gozhev.ru> / Summer 2018 / Moscow, Russia

#
# global input
#
LIVE555_EXTRA_FLAGS += $(LIBS_EXTRA_FLAGS)
LIVE555_ASSUME_ONE_ARCH ?= $(LIBS_ASSUME_ONE_ARCH)

#
# local state
#
LIVE555_DIR := third-party/live
LIVE555_CONFIG_DIR := config/live555
LIVE555_OUTPUT_DIR := $(BUILD_DIR)/$(LIVE555_DIR)

LIVE555_LIBS :=\
	$(LIVE555_OUTPUT_DIR)/liveMedia/libliveMedia.a\
	$(LIVE555_OUTPUT_DIR)/groupsock/libgroupsock.a\
	$(LIVE555_OUTPUT_DIR)/BasicUsageEnvironment/libBasicUsageEnvironment.a\
	$(LIVE555_OUTPUT_DIR)/UsageEnvironment/libUsageEnvironment.a\

LIVE555_CUSTOMIZED := $(LIVE555_DIR)/.customized
LIVE555_CONF_ARCH := armlinux-sn986

ifneq ($(realpath $(LIVE555_DIR)),$(realpath $(LIVE555_OUTPUT_DIR)))
LIVE555_OUT_OF_TREE := y
endif

ifdef LIVE555_EXTRA_FLAGS
LIVE555_MAKE_ARGS += EXTRA_COMPILE_OPTS='$(LIVE555_EXTRA_FLAGS)'
endif

define live555_customize
	cp -f $(LIVE555_CONFIG_DIR)/config.$(LIVE555_CONF_ARCH) $(LIVE555_DIR)
	cp -f $(LIVE555_CONFIG_DIR)/_.gitignore $(LIVE555_DIR)/.gitignore
	touch $(LIVE555_CUSTOMIZED)
endef

define live555_undo_customize
	rm -f $(LIVE555_DIR)/config.$(LIVE555_CONF_ARCH)
	rm -f $(LIVE555_DIR)/.gitignore
	rm -f $(LIVE555_CUSTOMIZED)
endef

define live555_make
	+$(MAKE) -C $(LIVE555_DIR) $(strip $1)
endef

define live555_make_cond
	$(if $(wildcard $(LIVE555_DIR)/Makefile),\
		+$(MAKE) -C $(LIVE555_DIR) $(strip $1))
endef

ifeq ($(LIVE555_OUT_OF_TREE),y)
define live555_copy_libs
	$(foreach x, $(LIVE555_LIBS),\
		mkdir -p $(dir $(x)) $(NL)\
		cp $(x:$(LIVE555_OUTPUT_DIR)/%=$(LIVE555_DIR)/%) $(x) $(NL))
endef
endif

ifeq ($(LIVE555_OUT_OF_TREE),y)
define live555_undo_copy_libs
	$(call rm_all, $(LIVE555_LIBS))
	$(call rm_empty_dirs, $(LIVE555_OUTPUT_DIR))
endef
endif

define live555_configure
	cd $(LIVE555_DIR) && ./genMakefiles $(LIVE555_CONF_ARCH)
endef

define live555_configure_cond
	$(if $(wildcard $(LIVE555_DIR)/Makefile),,\
		$(call live555_configure))
endef

ifneq ($(LIVE555_ASSUME_ONE_ARCH),y)
ifneq ($(wildcard $(LIVE555_DIR)/Makefile),)
define live555_check_source_tree
	$(error We are detected build artifacts in the live555 directory.\
		Live555 doesn't support out-of-tree builds so that you have\
		to either call 'make live555-distclean-self' or set\
		LIVE555_ASSUME_ONE_ARCH=y)
endef
endif
endif

#
# here is a workaround which implements
# 'single-receipt->multiple-targets' pattern;
# $(LIVE555_ALL) should be a file that will never exist
#
LIVE555_ALL := $(LIVE555_OUTPUT_DIR)/~ALL

$(LIVE555_LIBS): $(LIVE555_ALL) ;

.INTERMEDIATE: $(LIVE555_ALL)
$(LIVE555_ALL): $(LIVE555_CUSTOMIZED) | $(BUILD_DIR)
	$(call live555_check_source_tree)
	$(call live555_configure_cond)
	$(call live555_make, all $(LIVE555_MAKE_ARGS))
	$(call live555_copy_libs)

$(LIVE555_CUSTOMIZED):
	$(call live555_customize)

.PHONY: live555
live555: $(LIVE555_ALL)

#
# clean live555 source-tree directory and also remove live555 '.a' files
# from the 'build' directory
#
.PHONY: live555-clean
live555-clean:
	$(call live555_undo_copy_libs)
	$(call live555_make_cond, clean)

#
# the same as above plus undo any customizations
#
.PHONY: live555-distclean
live555-distclean:
	$(call live555_undo_copy_libs)
	$(call live555_make_cond, distclean)
	$(call live555_undo_customize)

#
# only clean live555 source-tree directory
#
.PHONY: live555-clean-self live555-distclean-self
live555-clean-self live555-distclean-self:
	$(call live555_make_cond, $(@:live555-%-self=%))

#
# global output
#
STATIC_LIBS += $(LIVE555_LIBS)

CPPFLAGS +=\
	-I$(LIVE555_DIR)/liveMedia/include\
	-I$(LIVE555_DIR)/groupsock/include\
	-I$(LIVE555_DIR)/BasicUsageEnvironment/include\
	-I$(LIVE555_DIR)/UsageEnvironment/include\

libs: live555
libs-clean: live555-clean
libs-distclean: live555-distclean
libs-clean-self: live555-clean-self
libs-distclean-self: live555-distclean-self

