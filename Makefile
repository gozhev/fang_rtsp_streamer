# © Mikhail Gozhev <dev@gozhev.ru> / Summer 2018 / Moscow, Russia
#
# Note that some variables defined at the most end of this file and so that
# should only be used in reciepts.
#
# Currently, sources are listed using relative paths so that
# Makefile should only be called from the source root directory.
#
# Note that we use CXX as a linker for this c/cxx project.
#

TOOLCHAIN_DIR ?= ../../../toolchain/crosstool-4.5.2/bin
SYSROOT_DIR ?= ../../../filesystem/rootfs
VENDOR_MIDDLEWARE_DIR ?= ../../../../vendor/middleware
VENDOR_AUTOCONF_DIR ?= ../../../vendor_autoconf

NAME := rtsp_streamer

SRCS :=\
	src/main.cc\
	src/live/SnxVideoUnicastSMSubsession.cc\
	src/live/SnxVideoSource.cc\

CPPFLAGS +=\
	-Isrc\
	-Isrc/live\
	-I$(VENDOR_AUTOCONF_DIR)/include\
	-I$(VENDOR_MIDDLEWARE_DIR)/include\
	-I$(VENDOR_MIDDLEWARE_DIR)/include/snx_vc\
	-I$(VENDOR_MIDDLEWARE_DIR)/include/snx_rc\
	-I$(VENDOR_MIDDLEWARE_DIR)/include/snx_isp\
#	-DDEBUG\

LIBS_EXTRA_FLAGS +=\
	-gstabs+\

LDFLAGS +=\
	-L$(SYSROOT_DIR)/lib\
	-Wl,-rpath-link,$(SYSROOT_DIR)/lib\
	-pthread\
	-gstabs+\
#	-O2\
#	-static\

LDLIBS +=\
	-lsnx_vc\
	-lsnx_rc\
	-lsnx_isp\
#	-lm\
#	-ldl\
#	-lasound\
#	-lsnx_audio\
#	-lavformat\
#	-lavcodec\
#	-lavutil\
#	-lz\

CXXFLAGS +=\
	-std=c++0x\
	-pthread\
	-gstabs+\
#	-O2\
#	-pedantic\

CFLAGS +=\
	-std=c99\
	-pedantic\
	-Wstrict-prototypes\
	-Wmissing-prototypes\
	-pthread\
	-gstabs+\
#	-O2\
#	-D_XOPEN_SOURCE=600\
#	-Wc++-compat\

#CXXFLAGS,CFLAGS +=\
#	-Wall\
#	-Wextra\
#	-Wformat=2\
#	-Wshadow\
#	-Wwrite-strings\
#	-Wundef\
#	-Wlogical-op\
#	-Werror\
#	-Wconversion\
#	-Wswitch-default\
#	-Wstrict-overflow=2\
#	-Wcast-align\
#	-Winit-self\
#	-Wcast-qual\
#	-Wduplicated-cond\
#	-Wdouble-promotion\
#	-Wnull-dereference

CROSS_COMPILE ?= arm-linux-
CC := $(CROSS_COMPILE)gcc
CXX := $(CROSS_COMPILE)g++
LD := $(CXX)
AS := $(CC)
PATH := $(abspath $(TOOLCHAIN_DIR)):$(PATH)
FOR_EXPORT += PATH CROSS_COMPILE

# where to put object files, an absolute path
BUILD_DIR := $(PWD)/build

# where to put target binaries, an absolute path
DEST_DIR := $(PWD)

# add here static libraries to be linked with the target binary
STATIC_LIBS +=

#
# If there are third-party libraries which do not support out-of-tree build
# then the default behavior is to make them in-place and then just copy
# binaries to the build tree. If the following variable is NOT set then
# you will get an error each time you will try to compile a library which
# directory is populated with artifacts from the previous build. You have to
# either set this variable or call 'make libs-distclean-self' after each
# library build.
#
LIBS_ASSUME_ONE_ARCH := y

#
# parse command line arguments
#
include config/make/args.mk

# select default target
all: elf

#
# redefine default elf name
#
ELF := $(DEST_DIR)/$(NAME)

#
# basic helper functions and default build targets
#
include config/make/helpers.mk
include config/make/build.mk

#
# library handling
#
include config/make/lib.mk
include config/live555/live555.mk

#
# elf handling
#
include config/make/elf.mk
include config/make/deps.mk

#
# export variables to subshells
#
include config/make/export.mk

#
# extra features
#
include config/make/tags.mk

#
# dirs creation/deletion handling
#
include config/make/dirs.mk

